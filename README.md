
# Update XSD

    curl https://ecosystem.atlassian.net/rest/atlassian-connect/latest/installer/schema/atlassian-plugin-remotable -o xml/jira.xsd
    curl https://ecosystem.atlassian.net/wiki/rest/atlassian-connect/latest/installer/schema/atlassian-plugin-remotable -o xml/confluence.xsd

# Upload docs

rsync -avz -e 'ssh' . uploads@developer-app.internal.atlassian.com:/opt/j2ee/domains/atlassian.com/developer-prod/static-content/static/connect

