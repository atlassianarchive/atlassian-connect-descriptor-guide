AJS.$(function($) {
  hljs.highlightBlock($('code')[0]);

  function toSlug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.replace(/[A-Z]/g, function(s){ return "-" + s; }); // camel case handling
    str = str.toLowerCase();
    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
      .replace(/\s+/g, '-') // collapse whitespace and replace by -
      .replace(/-+/g, '-') // collapse dashes
      .replace(/\-type$/,'') // remove -type
      .replace(/^\-/,''); // remove leading dash
    return str;
  }

  function normalizeObject(obj, isRoot){
    // resolve all child nodes and remove xs: namespacing first
    for(var el in obj){
      if (typeof obj[el] === 'object') {
        obj[el] = normalizeObject(obj[el]);
      }
      if (/^xs\:/.test(el)) {
        obj[el.replace(/^xs\:/,'')] = obj[el];
        delete obj[el];
      }
    }

    // clean up nodes
    for(var el in obj){
      if(el === 'complexContent') {
        try{
          for (var ext in obj['complexContent']['extension']){
            if(ext !== 'base'){
              obj[ext] = obj['complexContent']['extension'][ext];
            }
          }
          var extObj = getType(obj['complexContent']['extension']['base']);
          for (var ext in extObj){
            if(ext !== 'name' && ext !== 'slug' && ext !== 'type') {
              if (typeof obj[ext] === 'undefined') {
                obj[ext] = extObj[ext];
              } else {
                obj[ext] = extObj[ext].concat(obj[ext]);
              }
            }
          }
          delete obj['complexContent'];
        } catch(e) {
          console.error(e.message);
        }
      }
      if(el === 'simpleContent') {
        try{
          for (var ext in obj['simpleContent']['extension']){
            if(ext !== 'base'){
              obj[ext] = obj['simpleContent']['extension'][ext];
            }
          }
          var extObj = getType(obj['simpleContent']['extension']['base']);
          for (var ext in extObj){
            if(ext !== 'name' && ext !== 'slug' && ext !== 'type') {
              obj[ext] = extObj[ext];
            }
          }
          delete obj['simpleContent'];
        } catch(e) {
          console.error(e.message);
        }
      }
      if(el === 'use') {
        obj['required'] = obj[el] === 'required' ? true : false;
        delete obj[el];
      }
      if(el === 'type') {
        for(var el in obj){
          if(el === 'type'){
            try {
              var childType = getType(obj[el]);
              for(var typeProp in childType){
                if(typeProp !== 'slug' && typeProp !== 'name') {
                  obj[typeProp] = childType[typeProp];
                }
              }
              delete obj[el];
            } catch(e) {
              console.error(e.message);
            }
          }
        }
      }
      if(el === 'name'){
        obj['slug'] = toSlug(obj[el]);
      }
      if(el === 'sequence'){
        if(typeof obj[el]['element'] !== 'undefined'){
          for(var childEl in obj[el]['element']){
            if(typeof obj[el]['element'][childEl]['required'] === 'undefined'){
              if (obj[el]['element'][childEl].minOccurs === "0") {
                obj[el]['element'][childEl].required = false;
              } else {
                obj[el]['element'][childEl].required = true;
              }
            }
          }
          if(obj['element]']){
            obj['element'] = obj['element'].concat(obj[el]['element']);
          } else {
            obj['element'] = obj[el]['element'];
          }
        }
        if(typeof obj[el]['choice'] !== 'undefined'){
          for(var childEl in obj[el]['choice']['element']){
            if(obj[el]['choice']['element'][childEl]['required'] === 'undefined'){
              obj[el]['choice']['element'][childEl].required = false;
            }
          }
          if(typeof obj['element]'] !== 'undefined'){
            obj['element'] = obj['element'].concat(obj[el]['choice']['element']);
          } else {
            obj['element'] = obj[el]['choice']['element'];
          }
        }
        delete obj[el];
      }
      if(el === 'choice'){
        for(var childEl in obj[el]['element']){
          obj[el]['element'][childEl].required = false;
        }
        if(typeof obj['element'] === 'undefined') {
          obj['element'] = obj[el]['element'];
        } else {
          obj['element'] = obj['element'].concat(obj[el]['element'])
        }
        delete obj[el];
      }
      if(el === 'element') {
        obj[el] = [].concat(obj[el]);
      }
      if(el === 'attribute') {
        obj[el] = [].concat(obj[el]);
      }
      if(el === 'annotation' && typeof obj[el]['documentation'] !== 'undefined'){
        if(typeof obj[el]['documentation']['description'] === 'undefined'){
          obj.doc = obj[el]['documentation'];
          // delete obj[el];
        } else if(typeof obj[el]['documentation']['description'] !== 'undefined') {
          obj.doc = obj[el]['documentation']['description'];
          // delete obj[el];
        }
        if(typeof obj[el]['documentation']['resources'] !== 'undefined') {
          obj.resources = obj[el]['documentation']['resources'];
        }
        delete obj[el];
      }
    }
    if(isRoot) {
      if (typeof obj['doc'] === 'undefined') {
        var el = _.find(elements, function(o){
          return o.slug === obj.slug;
        });
        if (typeof el.doc !== 'undefined'){
          obj['doc'] = el.doc;
        }
      }
    }
    return obj;
  }

  function getType(name, isRoot){
    var cType = _.where(objectTypes,{name:name});
    cType = cType.length > 0 ? normalizeObject(cType[0], isRoot) : false;
    return cType;
  }
  window.gt = getType

  function getElements() {
    var q = [getType('AtlassianPluginType')];
    var el = [];
    while (q.length > 0) {
      var e = q.pop();
      if (typeof e.element !== 'undefined'){
        el.push(e);
        q = q.concat(e.element);
      } else {
        el.push(e);
      }
    }
    return el;
  }

  var schema = {};
  var objectTypes;
  function loadSchema(url,name, cb){
    if(typeof schema[name] === 'undefined'){
      $.ajax({
        url: url,
        dataType: 'text'
      }).done(function(xml){
        var xotree = new XML.ObjTree();
        xotree.attr_prefix = "";
        var tree = xotree.parseXML(xml);
        var root = tree['xs:schema'];
        objectTypes = [].concat(tree['xs:schema']['xs:complexType'])
          .concat(tree['xs:schema']['xs:simpleType']);
        schema[name] = objectTypes;
        if(cb) { cb() };
      });
    } else {
      objectTypes = schema[name];
      if(cb) { cb() };
    }
  }

  Handlebars.registerPartial("enums", $("#p3-enum-partial").html());
  Handlebars.registerHelper('enumsWithDoc', function(enums, options){
    if (!enums) return;
    if(enums.length > 0 && typeof enums[0]['doc'] !== 'undefined') {
      return options.fn(this);
    } else if (enums.length > 0 && typeof enums[0]['doc'] === 'undefined'){
      return options.inverse(this);
    } else if (enums.length === 0) {
      return false;
    }
  });
  Handlebars.registerHelper('inspect', function(){
    console.log(arguments)
  });
  var template = Handlebars.compile($('#p3-doc-template').html(), {data:true});
  var elements;
  function renderDoc(prop, cb){
    var typeName = prop
      .replace(/(\-[a-z])/g,function($1){return $1[1].toUpperCase()})
      .replace(/(^[a-z])/,function($1){return $1.toUpperCase()}) + "Type";
    elements = getElements();
    var obj = getType(typeName, true);
    if (obj) {
      $('#p3-doc-container').html(template(obj));
    }
    cb()
  }

  $('#p3-codepane [data-el]').on('click', function(el){
    app.trigger('typeNav', $(this));
    return false;
  });
  $('#p3-annotations').on('click','a.p3-el[data-el]', function(){
    app.trigger('typeNav', $(this));
    return false;
  });

  window.dialog = new AJS.Dialog({
    width:600,
    height:400,
    id:"resource-dialog",
    closeOnOutsideClick: true
  });
  dialog.addPanel("", "", "panel-body");
  dialog.addButton("Close", function (dialog) {
    dialog.hide();
  });
  // var dheader = dialog.
  var dpanel = dialog.getPanel(0);

  $('#p3-annotations').on('click','.enum-w-resource', function(){
    $this = $(this);
    dialog.addHeader("Resources for " + $this.text());
    dpanel.html($('<div>').append($this.next().clone().removeClass('hidden')).html());
    dialog.show();
    return false;
  });

  $('.aui-nav li').on('click', function(){
    var $this = $(this);
    if (!$this.hasClass('aui-nav-selected')) {
      $this.parent().find('li').removeClass('aui-nav-selected');
      $this.addClass('aui-nav-selected');
    }
  });

  // var $window = $(window);
  // var $doc = $(document);
  // var $codepaneWidth = $('#p3-codepane').width();
  // var $codepane = $('#p3-codepane').offset();
  // var $footer = $('footer');
  // var reset = 'affix affix-top affix-bottom';
  // $window.on('scroll', function(){
  //   var topOfFooter = $footer.outerHeight() - ($doc.height() - ($window.scrollTop() + $window.height()));
  //   if($window.scrollTop() > $codepane.top && topOfFooter < 0) {
  //     $('#p3-codepane').removeClass(reset).addClass('affix').width($codepaneWidth);
  //   } else if(topOfFooter >= 0) {
  //     $('#p3-codepane').removeClass(reset).addClass('affix-bottom');
  //   } else {
  //     $('#p3-codepane').removeClass(reset);
  //   }
  // });

  var App = Backbone.Router.extend({
    routes: {
      "": "index",
      ":app": "app",
      ":app/:type": "type"
    },
    initialize: function(){
      this.on({
        'typeNav': function(context){
          this.navigate([location.hash.replace('#','').split('/')[0],context.data('el')].join('/'), {trigger: true});
        },
        'route': function(route,params){
          $('.aui-nav .p3-selected').removeClass('p3-selected');
          $('.aui-nav a[href="#'+params[0]+'"]').addClass('p3-selected');
        }
      });
    },
    "index": function(){
      this.navigate('jira/atlassian-plugin', {trigger: true});
    },
    "app": function(app){
      this.navigate(app+'/atlassian-plugin', {trigger: true});
    },
    "type": function(app,type){
      var url = "xml/jira.xsd";
      if (app !== 'jira') {
        url = "xml/confluence.xsd";
      }
      loadSchema(url, app, function(){
        $('#p3-codepane [data-el]').removeClass('p3-xml-section-selected');
        $('#p3-codepane [data-el="'+type+'"]').addClass('p3-xml-section-selected');
        renderDoc(type, function(){
          $(window).scrollTop(0)
        });
      });
    },
  });
  var app = new App;

  Backbone.history.start();
});